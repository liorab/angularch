import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private db:AngularFirestore) { }

  userCollection:AngularFirestoreCollection=this.db.collection('users');
bookCollection:AngularFirestoreCollection=this.db.collection('books');

  getBooks(userId:string){
    //with no users
    //return this.bookCollection.valueChanges({idField:'id'});
    this.bookCollection= this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(
      //pipe מאפשר לשרשר פונקציות
      map(
        //map גם מאפשר לשרשר פונקציות
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id= document.payload.doc.id;
            return data;
          }
        )
      )
    )
  }
  addBook(userId:string,title_input:string,author_input:string){
    const book = {title:title_input, author:author_input};
    this.bookCollection.add(book);
  }
  getBook(id:string,userId:string):Observable<any>{
    //return this.db.doc(`books/${id}`).get();
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }
  updateBook(userId:string,title:string,author:string,id:string){
    this.db.doc(`users/${userId}/books/${id}`).update({
      title:title,
      author:author
    })
  }
  deleteBook(id:string,userId:string){
    this.db.doc(`users/${userId}/books/${id}`).delete();
  }
}
