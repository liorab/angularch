import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>
  ngZone: any;

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
      this.user=this.afAuth.authState;
     }

signup(email:string,password:string){
  this.afAuth.auth.createUserWithEmailAndPassword(email,password)
  .then(()=>{
    this.router.navigate(['/books']);
  }).catch((error) => {
    window.alert(error.message)
  })    
} 

login(email:string,password:string){
  return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
            this.router.navigate(['/books']);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
Logout(){
  this.afAuth.auth.signOut().then(res=> console.log('successful logout'));
}
}
