import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-temp-form',
  templateUrl: './temp-form.component.html',
  styleUrls: ['./temp-form.component.css']
})
export class TempFormComponent implements OnInit {

  cities:object[] = [{id:1, name:'Jerusalem'},{id:2, name:'London'},
  {id:3, name:'Paris'}, {id:4, name:'non-exitent'}];
//temperature:number;
city:string; 

onSubmit(){
this.router.navigate(['/temperature', this.city]);
}

constructor(private router: Router) { }

ngOnInit() {
}

}
