import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import { RouterModule, Routes } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import { FormsModule }   from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AddBookComponent } from './add-book/add-book.component';
import { TemperatureComponent } from './temperature/temperature.component';
import { TempFormComponent } from './temp-form/temp-form.component';
import { HttpClientModule } from '@angular/common/http';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';





const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'AddBook', component: AddBookComponent },
  { path: 'AddBook/:id', component: AddBookComponent },
  { path: 'temperature/:city', component: TemperatureComponent},
  { path: 'tempform', component: TempFormComponent },
  { path: 'docform', component: DocformComponent },
  { path: 'classified', component: ClassifiedComponent },
  { path: "",
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    SignupComponent,
    LoginComponent,
    AddBookComponent,
    TemperatureComponent,
    TempFormComponent,
    ClassifiedComponent,
    DocformComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatExpansionModule,
    FormsModule,
    BrowserModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ) 
  ],
  providers: [AngularFireAuth ],
  bootstrap: [AppComponent]
})
export class AppModule { }
