export interface Weather {
        name:String,
        country:String,
        image:String,
        description:String,
        temperature:number,
        lat?:number,
        lon?:number
}