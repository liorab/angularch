import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  constructor(public booksService:BooksService, public router:Router, public route:ActivatedRoute, public authService:AuthService) { }

  title:string;
  author:string;
  buttonText:string="Add Book";
  isEdit=false;
  id:string;
  userId:string;

  ngOnInit() {
    this.id=this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id){
          this.isEdit=true;
          this.buttonText="Update book";
          this.booksService.getBook(this.id,this.userId).subscribe(
            book=> {
              this.author=book.data().author;
              this.title=book.data().title;
            }
          )
        }
      }  
    )
  }
  onSubmit(){
    if(this.isEdit){
      this.booksService.updateBook(this.userId,this.title,this.author,this.id);
    }
    else{
      this.booksService.addBook(this.userId,this.title,this.author);
    }
    this.router.navigate(['/books']);
  }

 
}
