import { Router } from '@angular/router';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docform',
  templateUrl: './docform.component.html',
  styleUrls: ['./docform.component.css']
})
export class DocformComponent implements OnInit {

  constructor(public classifyService:ClassifyService,public router:Router) { }

  text:string;

  ngOnInit() {
  }

  onSubmit(){
    this.classifyService.doc=this.text;
    this.router.navigate(['/classified'])
  }

}
