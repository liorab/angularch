import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(public booksservise:BooksService,public authService:AuthService) { }
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  books$:Observable<any>;
  userId:string;


  ngOnInit() {
    //where are no users to each book
     //this.books$=this.booksservise.getBooks();
     //with users
     this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.booksservise.getBooks(this.userId);      
        })
   
  }
  deleteBook(id:string){
    this.booksservise.deleteBook(id,this.userId);
  }

}
