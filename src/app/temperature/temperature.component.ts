import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { TempService } from '../temp.service';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit {
likes:number=0;
city:string;
temperature:number;
image:String;
tempData$:Observable<Weather>;

  constructor(private route: ActivatedRoute,private  tempservice:TempService) { }

  ngOnInit() {
   // this.temperature = this.route.snapshot.params.temperature;
   this.city= this.route.snapshot.params.city;
   this.tempData$=this.tempservice.searchWeatherData(this.city);
   this.tempData$.subscribe(
     data=>{
        console.log(data);
         this.temperature=data.temperature;
         this.image=data.image;
      }
    )
   }
 
  addLikes(){
this.likes++;
  }

}