import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

//public url="https://zeouxsy751.execute-api.us-east-1.amazonaws.com/beta";
 private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};
  public doc:string;

  classify():Observable<any>{
    let json = {
      "articles":[
        {"text":this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
     map(res=>{
       let final= res.body.replace('[','');
       final= final.replace(']','');
       return final;
     })
    ) 
  }

  constructor(private http:HttpClient, public db:AngularFirestore) { }

  classifyCollection:AngularFirestoreCollection=this.db.collection('classify');

  saveClassidy(doc:string, category:string){
    console.log(doc,category);
    //const classify = {doc:doc, category:category};
    this.classifyCollection.add({doc:doc, category:category});
  }
}
